package praktikum4;

import java.util.Scanner;

public class Motherboard {
	
	static class USB
	{
		int USB2;
		int USB3;
		int getTotalPorts()
		{
			return USB2 + USB3;}
	}

	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			Motherboard.USB usb = new Motherboard.USB();
			usb.USB2 = input.nextInt();
			usb.USB3 = input.nextInt();
			System.out.println("Total Ports = "+usb.getTotalPorts());
		}
	}

}
