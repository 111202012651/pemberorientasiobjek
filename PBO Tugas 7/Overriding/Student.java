package Overriding;

//inherit dari Person
public class Student extends Person{
	String nim;
	
	// method baru di subclass
	public String getNim() {
		return nim;
	}
	
	@Override
	public void identity() {
		System.out.print("NIM: " +getNim());
	}
	
}
