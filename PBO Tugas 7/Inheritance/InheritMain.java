package Inheritance;

public class InheritMain {
	public static void main(String[] args) {
		Student mahasiswa  = new Student();
		//Pemanggilan method dari superclass
		mahasiswa.identity();
		//pemangilan method dari subclass
		String nim = mahasiswa.getNim();
		System.out.println(nim);
}
}
