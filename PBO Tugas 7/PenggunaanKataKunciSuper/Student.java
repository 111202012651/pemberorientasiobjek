package PenggunaanKataKunciSuper;


//inherit dari Person
public class Student extends Person{
	String nim;
	
	// method baru di subclass
	public String getNim() {
		return nim;
	}
	
	@Override
	public void identity() {
		super.identity();
		System.out.print("NIM: " +getNim());
	}
	
}
